# Serverless Route Plan API

A serverless REST API that fetches a route plan based on coordinates from the OpenTripPlanner2 server and amends calculated CO2 emissions to the response.

## Technologies Used

-   TypeScript
-   Serverless framework
-   AWS Lambda & Amazon API Gateway
-   Node v16.x

## Scope Covered

The implemented solution covers the following aspects:

-   Implementing functions for route planning and CO2 emission calculation.
-   Writing comprehensive unit tests for all the functions using the TDD approach.
-   Automating tests before deploying with the Serverless framework.

## Decisions and Trade-offs

During the implementation, certain decisions and trade-offs were made:

-   Test-Driven Development (TDD) was used to ensure code correctness and maintainability.
-   The code is written in TypeScript to leverage its static typing and enhanced development experience.
-   The `serverless.yml` file was kept as a single service for the purpose of this exercise.

## Notes

-   This project was developed as part of an exercise(check `./exercise-file.pdf`, and it was the first time using Lambda and the Serverless framework.
-   Due to the unavailability of the API server, the solution was based on the OpenTripPlanner documentation without directly testing the API integration.

![unavailable API server](https://i.ibb.co/PzT4dPF/Screen-Shot-2023-06-26-at-03-31-48.png)

## Areas for Improvement

Given more time, the following areas could be improved:

-   Enhanced error handling and error messages to provide more detailed information about the encountered errors.
-   Integration tests to ensure the functionality of the serverless handler in a real-world scenario.
-   More extensive validation and error checking for input parameters and responses.
-   Separation of `serverless.yml` into multiple services for better organization and scalability.

## Dependencies Used

The solution has the following dependencies specified in the `package.json` file:
![package.json](https://i.ibb.co/BPnQRcf/Screen-Shot-2023-06-26-at-02-59-16.png)

## Running Tests

To run the code, follow these steps:

1. Ensure that Node.js and npm are installed on your machine.
2. Clone the repository and navigate to the project directory.
3. Install the dependencies by running the following command:
   `npm install`
4. Run the tests using the following command:
   `npm test`

This will execute the unit tests and display the test results.

## Deploying to AWS using Serverless

To deploy the code to AWS using the Serverless framework, follow these steps:

1. Install the Serverless Framework globally by running the following command:
   `npm install -g serverless`
2. Install dependencies by running:
   `npm install`
3. Configure your AWS credentials by following the Serverless [Documentation](https://www.serverless.com/framework/docs/providers/aws/guide/credentials)

4. Create an `env.json` file with the following content:

```json
{
    "OPEN_TRIP_PLANNER_API": "REPLACE_ME",
    "OPEN_TRIP_PLANNER_API_KEY": "REPLACE_ME"
}
```

Replace `"REPLACE_ME"` with the actual values for your OpenTripPlanner API configuration.

5. Deploy the service to AWS using the following command:
   `npm run deploy:prod`  
   **Note**: This will deploy the service if all the tests are successfully passed ✔.

6. Send a GET request to `${DEPLOYED-API-ENDPOINT}` with the following query parameters:
    - `fromPlace`: Starting point coordinates (latitude, longitude).
    - `toPlace`: Destination coordinates (latitude, longitude).

Example curl command:  
`curl -X GET "${DEPLOYED-API-ENDPOINT}?fromPlace=60.148156622692035,24.987887975719225&toPlace=60.200309580474354,25.15206098556519"`

Please make sure to replace `${DEPLOYED-API-ENDPOINT}` with the actual endpoint URL once the service is deployed.
