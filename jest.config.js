/** @type {import('ts-jest').JestConfigWithTsJest} */
const { pathsToModuleNameMapper } = require("ts-jest");
const { compilerOptions } = require("./tsconfig.paths.json");

module.exports = {
    preset: "ts-jest",
    testEnvironment: "node",
    moduleNameMapper: { ...pathsToModuleNameMapper(compilerOptions.paths), axios: "axios/dist/node/axios.cjs" },
    modulePaths: ["./"],
    transform: {
        "^.+\\.ts?$": "ts-jest",
    },
    transformIgnorePatterns: ["<rootDir>/node_modules/"],
};
