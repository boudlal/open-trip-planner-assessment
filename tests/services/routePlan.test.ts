import axios from "axios";
import * as routePlanService from "@services/routePlan";
import { Leg, Itinerary } from "@customTypes/routePlanTypes";
import {
    createAxiosResponseMock,
    createLegsMock,
    createOpenTripPlannerItineraryResponseMock,
    createOpenTripPlannerLegResponseMock,
    createRoutPlanResponse,
    createRoutePlanEventMock,
} from "tests/mocks/routePlan";
import { FromSchema } from "json-schema-to-ts";
import schema from "@schemas/routePlan";

describe("RoutePlan Service", () => {
    afterEach(() => {
        jest.clearAllMocks();
        jest.restoreAllMocks();
    });

    describe("getRoutePlan", () => {
        it("should be defined", () => {
            expect(routePlanService.getRoutePlan).toBeDefined();
        });

        it("should return the route plan when successful", async () => {
            // GIVEN
            const queryParams = createRoutePlanEventMock().queryStringParameters as FromSchema<typeof schema>;
            const expectedPlan = createRoutPlanResponse();
            const expectedPlanResponse = createAxiosResponseMock({ plan: expectedPlan });
            jest.spyOn(routePlanService, "getPlan").mockResolvedValue(expectedPlanResponse);

            // WHEN
            const result = await routePlanService.getRoutePlan(queryParams);

            // THEN
            expect(result).toEqual({
                plan: expectedPlan,
            });
        });

        it("should return an error response if plan object does not exist in the getPlan response", async () => {
            // GIVEN
            const queryParams = createRoutePlanEventMock().queryStringParameters as FromSchema<typeof schema>;
            const expectedPlanResponse = createAxiosResponseMock({ plan: undefined });
            jest.spyOn(routePlanService, "getPlan").mockResolvedValue(expectedPlanResponse);

            // WHEN
            const result = await routePlanService.getRoutePlan(queryParams);

            // THEN
            expect(result).toEqual({
                error: "Something went wrong",
            });
        });

        it("should handle errors and return an error response", async () => {
            // GIVEN
            const queryParams = createRoutePlanEventMock().queryStringParameters as FromSchema<typeof schema>;
            const error = new Error("Internal server error");
            jest.spyOn(routePlanService, "getPlan").mockImplementation(() => {
                throw error;
            });

            // WHEN
            const result = await routePlanService.getRoutePlan(queryParams);

            // THEN
            expect(result).toEqual({
                error: "Internal server error",
            });
        });
    });
    describe("getPlan", () => {
        it("should be defined", () => {
            expect(routePlanService.getPlan).toBeDefined();
        });
        it("should make a GET request and return the response", async () => {
            // GIVEN
            const eventMock = createRoutePlanEventMock();
            const queryParams = eventMock.queryStringParameters;
            const expectedResponse = createAxiosResponseMock();
            jest.spyOn(axios, "get").mockResolvedValue(expectedResponse);
            // WHEN
            const response = await routePlanService.getPlan(queryParams);
            // THEN
            expect(response).toEqual(expectedResponse);
            expect(axios.get).toHaveBeenCalledWith(process.env.OPEN_TRIP_PLANNER_API, {
                params: {
                    arriveBy: false,
                    wheelchair: false,
                    showIntermediateStops: true,
                    locale: "en",
                    ...queryParams,
                },
                headers: {
                    "x-api-key": process.env.OPEN_TRIP_PLANNER_API_KEY,
                },
            });
        });
        it("should throw an error if request fails and axios throws an error", async () => {
            // GIVEN
            const eventMock = createRoutePlanEventMock();
            const queryParams = eventMock.queryStringParameters;
            jest.spyOn(routePlanService, "getPlan").mockImplementation(() => {
                throw new Error();
            });
            // WHEN
            const getPlanRequest = async () => await routePlanService.getPlan(queryParams);
            // THEN
            await expect(getPlanRequest()).rejects.toThrow();
        });
    });
    describe("calculateCO2Emission", () => {
        it("should be defined", () => {
            expect(routePlanService.calculateCO2Emission).toBeDefined();
        });
        it("should calculate the total CO2 emission for legs", () => {
            // GIVEN
            const legs = createLegsMock();
            // WHEN
            const totalCO2 = routePlanService.calculateCO2Emission(legs);
            // THEN
            const expectedCO2 = 74 * 10 + 0 * 5;
            expect(totalCO2).toBe(expectedCO2);
        });
        it("should handle case sensitivity of leg.mode and calculate the total CO2 emission for legs", () => {
            // GIVEN
            let legs = createLegsMock();
            legs[0].mode = legs[0].mode.toLowerCase();
            legs[1].mode = legs[1].mode.toLowerCase();
            // WHEN
            const totalCO2 = routePlanService.calculateCO2Emission(legs);
            // THEN
            const expectedCO2 = 74 * 10 + 0 * 5;
            expect(totalCO2).toBe(expectedCO2);
        });
        it("should handle legs with unknown modes and not calculate CO2 emission for them", () => {
            //GIVEN
            const legs: Leg[] = createLegsMock([
                {
                    mode: "UNKNOWN_MODE",
                    distance: 5,
                },
            ]);
            // WHEN
            const totalCO2 = routePlanService.calculateCO2Emission(legs);
            // THEN
            const expectedCO2 = 74 * 10; // CO2 multiplier for BUS: 74
            expect(totalCO2).toBe(expectedCO2);
        });
    });

    describe("formatItinerary", () => {
        it("should be defined", () => {
            expect(routePlanService.formatItinerary).toBeDefined();
        });

        it("should format the itinerary according to the response schema and call formatLeg", () => {
            // GIVEN
            const legs = createLegsMock();
            const itineraryMock = createOpenTripPlannerItineraryResponseMock(legs) as Itinerary;
            const formatLegSpy = jest.spyOn(routePlanService, "formatLeg").mockImplementation((leg) => leg);

            // WHEN
            const result = routePlanService.formatItinerary(itineraryMock);

            // THEN
            const expectedOutput = {
                co2: itineraryMock.co2,
                distance: itineraryMock.distance,
                startTime: itineraryMock.startTime,
                endTime: itineraryMock.endTime,
                duration: itineraryMock.duration,
                legs: legs,
            };
            expect(formatLegSpy).toBeCalledTimes(legs.length);
            expect(result).toEqual(expectedOutput);
        });
    });

    describe("formatLeg", () => {
        it("should be defined", () => {
            expect(routePlanService.formatLeg).toBeDefined();
        });

        it("should format the leg according to the response schema", () => {
            // GIVEN
            const legMock = createOpenTripPlannerLegResponseMock() as Leg;

            // WHEN
            const result = routePlanService.formatLeg(legMock);

            // THEN
            const expectedOutput = {
                mode: "BUS",
                distance: 12345,
                co2: null,
            };
            expect(result).toEqual(expectedOutput);
        });
    });
});
