import * as handler from "@functions/routePlan";
import * as routePlanService from "@services/routePlan";
import { createContextMock, createRoutPlanResponse, createRoutePlanEventMock } from "@mocks/routePlan";

describe("RoutePlan Handler", () => {
    afterEach(() => {
        jest.clearAllMocks();
        jest.restoreAllMocks();
    });

    it("should be defined", () => {
        expect(handler.routePlan).toBeDefined();
    });

    it("should call getRoutePlan with the provided query parameters and return the response", async () => {
        // GIVEN
        const plan = createRoutPlanResponse();
        const getRoutePlanSpy = jest.spyOn(routePlanService, "getRoutePlan").mockResolvedValue({ plan });
        const eventMock = createRoutePlanEventMock();
        const contextMock = createContextMock();

        // WHEN
        const response = await handler.routePlan(eventMock, contextMock, null);

        // THEN
        const expectedResponse = {
            statusCode: 200,
            body: JSON.stringify({ plan }),
        };
        expect(response).toEqual(expectedResponse);
        expect(getRoutePlanSpy).toHaveBeenCalledWith(eventMock.queryStringParameters);
    });
    it("should return a 400 status code and error if required query parameters are missing", async () => {
        // GIVEN
        const plan = createRoutPlanResponse();
        const getRoutePlanSpy = jest.spyOn(routePlanService, "getRoutePlan").mockResolvedValue({ plan });
        const eventMock = createRoutePlanEventMock();
        eventMock.queryStringParameters.fromPlace = undefined; // fromPlace is undefined
        const contextMock = createContextMock();

        // WHEN
        const response = await handler.routePlan(eventMock, contextMock, null);

        // THEN
        const expectedResponse = {
            statusCode: 400,
            body: JSON.stringify({ error: "Bad request" }),
        };
        expect(response).toEqual(expectedResponse);
        expect(getRoutePlanSpy).not.toBeCalled();
    });

    it("should return a 500 status code  and error if an error occurs", async () => {
        // GIVEN
        jest.spyOn(routePlanService, "getRoutePlan").mockImplementation(() => {
            throw Error();
        });
        const eventMock = createRoutePlanEventMock();
        const contextMock = createContextMock();

        // WHEN
        const response = await handler.routePlan(eventMock, contextMock, null);

        // THEN
        const expectedResponse = {
            statusCode: 500,
            body: JSON.stringify({ error: "Something went wrong" }),
        };
        expect(response).toEqual(expectedResponse);
    });
});
