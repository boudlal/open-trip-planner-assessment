import schema from "@schemas/routePlan";
import { Leg, RoutePlan } from "@customTypes/routePlanTypes";
import { AxiosRequestHeaders, AxiosResponse } from "axios";
import { ValidatedAPIGatewayProxyEvent } from "@customTypes/globalTypes";
import { Context } from "aws-lambda";

export const createContextMock = (): Context => {
    return {} as Context;
};

export const createRoutePlanEventMock = (
    event?: ValidatedAPIGatewayProxyEvent<typeof schema>,
): ValidatedAPIGatewayProxyEvent<typeof schema> => {
    return {
        headers: {
            "Content-Type": "application/json",
        },
        queryStringParameters: {
            fromPlace: "60.148156622692035,24.987887975719225",
            toPlace: "60.200309580474354, 25.15206098556519",
            mode: "transit",
        },
        body: "",
        httpMethod: "GET",
        ...event,
    };
};

export const createLegsMock = (legs: Leg[] = []): Leg[] => {
    return [
        {
            mode: "BUS",
            distance: 10,
        },
        {
            mode: "WALK",
            distance: 5,
        },
        ...legs,
    ];
};

export const createAxiosResponseMock = (data: Record<string, any> = {}): AxiosResponse => {
    return {
        data,
        status: 200,
        statusText: "OK",
        headers: {},
        config: {
            headers: {} as AxiosRequestHeaders,
        },
    };
};

export const createRoutPlanResponse = (routePlan?: RoutePlan): RoutePlan => {
    return {
        from: {
            coordinates: [0, 0],
        },
        to: {
            coordinates: [0, 0],
        },
        itineraries: [
            {
                distance: 10,
                startTime: "10:00 AM",
                endTime: "11:00 AM",
                duration: 60,
                legs: [
                    {
                        mode: "BUS",
                        distance: 5,
                        co2: 370,
                    },
                    {
                        mode: "WALK",
                        distance: 5,
                    },
                ],
            },
        ],
        ...routePlan,
    };
};

export const createOpenTripPlannerLegResponseMock = (): Record<string, any> => {
    return {
        startTime: 12345,
        endTime: 12345,
        departureDelay: 12345,
        arrivalDelay: 12345,
        realTime: true,
        isNonExactFrequency: true,
        headway: 12345,
        distance: 12345.0,
        pathway: true,
        mode: "BUS",
        transitLeg: true,
        route: "...",
        agencyName: "...",
        agencyUrl: "...",
        agencyBrandingUrl: "...",
        agencyTimeZoneOffset: 12345,
        routeColor: "...",
        routeType: 12345,
        routeId: "...",
        routeTextColor: "...",
        interlineWithPreviousLeg: true,
        tripShortName: "...",
        tripBlockId: "...",
        headsign: "...",
        agencyId: "...",
        tripId: "...",
        serviceDate: "...",
        routeBrandingUrl: "...",
        from: {},
        to: {},
        intermediateStops: [],
        legGeometry: {},
        steps: [],
        alerts: [],
        routeShortName: "...",
        routeLongName: "...",
        boardRule: "...",
        alightRule: "...",
        rentedBike: true,
        duration: 12345.0,
    };
};

export const createOpenTripPlannerItineraryResponseMock = (legs: Leg[] = []): Record<string, any> => {
    return {
        duration: 12345,
        startTime: 12345,
        endTime: 12345,
        walkTime: 12345,
        transitTime: 12345,
        waitingTime: 12345,
        walkDistance: 12345.0,
        walkLimitExceeded: true,
        elevationLost: 12345.0,
        elevationGained: 12345.0,
        transfers: 12345,
        fare: {},
        legs,
        systemNotices: [],
        tooSloped: true,
    };
};
