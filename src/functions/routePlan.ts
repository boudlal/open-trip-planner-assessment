import { ValidatedEventAPIGatewayProxyEvent } from "@customTypes/globalTypes";
import schema from "@schemas/routePlan";
import { getRoutePlan } from "@services/routePlan";

export const routePlan: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
    try {
        const { fromPlace, toPlace } = event.queryStringParameters;

        if (!fromPlace || !toPlace) return { statusCode: 400, body: JSON.stringify({ error: "Bad request" }) };

        const { plan, error } = await getRoutePlan(event.queryStringParameters);
        if (plan) {
            return {
                statusCode: 200,
                body: JSON.stringify({
                    plan,
                }),
            };
        }

        return { statusCode: 500, body: JSON.stringify({ error }) };
    } catch (error) {
        return { statusCode: 500, body: JSON.stringify({ error: "Something went wrong" }) };
    }
};
