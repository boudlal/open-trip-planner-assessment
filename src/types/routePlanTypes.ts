export enum MultipliersEnum {
    AIRPLANE = 144,
    BICYCLE = 0,
    BICYCLE_RENT = 0,
    BUS = 74,
    CAR = 160,
    ESCOOTER = 110,
    FERRY = 144,
    FUNICULAR = 54,
    METRO = 54,
    PUBLIC_TRANSIT = 74,
    RAIL = 14,
    SCOOTER = 75,
    SHARED_BICYCLE = 0,
    SUBWAY = 74,
    TAXI = 250,
    TRAIN = 14,
    TRAM = 54,
    WAIT = 0,
    WALK = 0,
}

export interface Leg {
    mode: string;
    distance: number;
    co2?: number;
}

export interface Itinerary {
    co2?: number;
    distance: number;
    startTime: string;
    endTime: string;
    duration: number;
    legs: Leg[];
}

export interface RoutePlan {
    from: {
        coordinates: [number, number];
    };
    to: {
        coordinates: [number, number];
    };
    itineraries: Itinerary[];
}
