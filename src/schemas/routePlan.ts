import { MultipliersEnum } from "@customTypes/routePlanTypes";

export default {
    $schema: "http://json-schema.org/draft-07/schema#",
    type: "object",
    properties: {
        fromPlace: {
            type: "string",
        },
        toPlace: {
            type: "string",
        },
        mode: {
            type: "string",
            enum: Object.keys(MultipliersEnum).filter((key) => isNaN(Number(key))),
        },
        time: {
            type: "string",
        },
        arriveBy: {
            type: "boolean",
        },
        wheelchair: {
            type: "string",
        },
        showIntermediateStops: {
            type: "string",
        },
        locale: {
            type: "string",
        },
    },
    additionalProperties: false,

    required: ["fromPlace", "toPlace"],
} as const;
