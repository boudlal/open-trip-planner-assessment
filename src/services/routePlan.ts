import { FromSchema } from "json-schema-to-ts";
import schema from "@schemas/routePlan";
import { Itinerary, Leg, MultipliersEnum, RoutePlan } from "@customTypes/routePlanTypes";
import axios, { AxiosResponse } from "axios";

/**
 * Retrieves a route plan based on the provided data.
 *
 * @param {FromSchema<typeof schema>} data - The input data containing the route plan details.
 * @returns {Promise<{ statusCode: number, body: string }>} - The response containing the plan or error.
 */
export const getRoutePlan = async (data: FromSchema<typeof schema>): Promise<{ plan?: RoutePlan; error?: string }> => {
    try {
        const response = await getPlan(data);

        const { plan } = response.data as { plan: RoutePlan };
        if (!plan) return { error: "Something went wrong" };

        if (plan && plan.itineraries) {
            plan.itineraries.forEach((itinerary) => {
                itinerary.co2 = calculateCO2Emission(itinerary.legs);
            });
        }

        return {
            plan,
        };
    } catch (error) {
        // console.error("Error:", error);
        return { error: "Internal server error" };
    }
};

/**
 * Retrieves a plan from the API based on the provided locations.
 *
 * @param {string} fromPlace - The starting location.
 * @param {string} toPlace - The destination location.
 * @returns {Promise<AxiosResponse>} - The API response.
 */

export const getPlan = async (params: FromSchema<typeof schema>): Promise<AxiosResponse> => {
    const response = await axios.get(process.env.OPEN_TRIP_PLANNER_API, {
        params: {
            arriveBy: false,
            wheelchair: false,
            showIntermediateStops: true,
            locale: "en",
            ...params,
        },
        headers: {
            "x-api-key": process.env.OPEN_TRIP_PLANNER_API_KEY,
        },
    });
    return response;
};

/**
 * Calculates the CO2 emission based on the legs of the itinerary and attaches the CO2 value to each leg.
 *
 * @param {Leg[]} legs - The legs of the itinerary.
 * @returns {number} - The total CO2 emission.
 */
export function calculateCO2Emission(legs: Leg[]): number {
    let totalCO2 = 0;

    legs.forEach((leg) => {
        const mode = leg.mode.toUpperCase();
        if (MultipliersEnum.hasOwnProperty(mode)) {
            const co2Multiplier = MultipliersEnum[mode];
            leg.co2 = co2Multiplier * leg.distance;
            totalCO2 += leg.co2;
        }
    });
    return totalCO2;
}

/**
 * Formats an itinerary object by selecting specific properties and formatting the legs.
 *
 * @param {Itinerary} itinerary - The itinerary object to format.
 * @returns {Itinerary} The formatted itinerary object.
 */
export const formatItinerary = (itinerary: Itinerary): Itinerary => {
    return {
        co2: itinerary.co2,
        distance: itinerary.distance,
        startTime: itinerary.startTime,
        endTime: itinerary.endTime,
        duration: itinerary.duration,
        legs: itinerary.legs.map((leg) => formatLeg(leg)),
    };
};

/**
 * Formats a leg object by selecting specific properties and setting the CO2 to null if not present.
 *
 * @param {Leg} leg - The leg object to format.
 * @returns {Leg} The formatted leg object.
 */
export const formatLeg = (leg: Leg): Leg => {
    return {
        mode: leg.mode,
        distance: leg.distance,
        co2: leg.co2 || null,
    };
};
